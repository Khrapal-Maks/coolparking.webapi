﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ICalculation _calculationService;
        private readonly ILogService _logService;
        private readonly ITimerService _logTimer;
        private readonly ITimerService _withdrawTimer;

        public List<TransactionInfo> Transactions { get; }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            Transactions = new List<TransactionInfo>();
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _calculationService = new CalculationService();
            GetTimers();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle == null)
            {
                throw new ArgumentException("Invalid vehicle");
            }

            if (Parking.Source.AllTransports.Select(x => x.Id).Contains(vehicle.Id))
            {
                throw new ArgumentException();
            }

            if (ValidationParkingCapacity())
            {
                Parking.Source.AllTransports.Add(vehicle);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Transport is parked: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"\nID:      {vehicle.Id}\nType:    {vehicle.VehicleType}\nBalance: {vehicle.Balance}");
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Dispose()
        {
            Parking.Source.Dispose();
        }

        public decimal GetBalance()
        {
            return Parking.Source.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Source.AllTransports.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.Source.AllTransports.Capacity - Parking.Source.AllTransports.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new(Parking.Source.AllTransports);
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!Parking.Source.AllTransports.Select(x => x.Id).Contains(vehicleId))
            {
                throw new ArgumentException("Not found ID");
            }

            if (Parking.Source.AllTransports.Find(x => x.Id == vehicleId).Balance >= 0)
            {
                Parking.Source.AllTransports.Remove(Parking.Source.AllTransports.Find(x => x.Id == vehicleId));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Transport was issued");
            }
            else
            {
                throw new InvalidOperationException();
            }
        }


        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
            {
                throw new ArgumentException();
            }

            if (!Parking.Source.AllTransports.Select(x => x.Id).Contains(vehicleId))
            {
                throw new ArgumentException();
            }
            Parking.Source.AllTransports.Find(x => x.Id == vehicleId).Balance += sum;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("The balance has been replenished");
        }

        private void GetTimers()
        {
            _withdrawTimer.Elapsed += WithdrawTimer_WriteOff;
            _withdrawTimer.Start();
            _logTimer.Elapsed += LogTimer_WriteOff;
            _logTimer.Start();
        }

        private void WithdrawTimer_WriteOff(object sender, ElapsedEventArgs e)
        {
            foreach (Vehicle itemAllTransport in Parking.Source.AllTransports)
            {
                Transactions.Add(_calculationService.WeCalculatePayment(itemAllTransport, DateTime.Now));
            }
        }

        private void LogTimer_WriteOff(object sender, ElapsedEventArgs e)
        {
            StringBuilder text = new($"New logging period {DateTime.Now} Balance Parking: {Parking.Source.Balance} Sum all transactions: {Parking.Source.ParkingService.Transactions.Sum(x => x.Sum)}");
            foreach (TransactionInfo itemTransaction in Transactions.ToList())
            {
                text.Append($"*{itemTransaction.DateTime}  with ID:  {itemTransaction.Id}  the money was written off in the amount:  {itemTransaction.Sum}.");
            }
            _logService.Write(text.ToString());
            Transactions.Clear();
        }

        private static bool ValidationParkingCapacity()
        {
            return Parking.Source.AllTransports.Count < Settings.Capacity;
        }
    }
}