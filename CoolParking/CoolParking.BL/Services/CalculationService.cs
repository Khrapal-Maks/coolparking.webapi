﻿using System;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class CalculationService : ICalculation
    { 
        private decimal _tariff;     

        public TransactionInfo WeCalculatePayment(Vehicle vehicle, DateTime dateTime)
        {
            GetTarrif(vehicle);
            return new TransactionInfo(dateTime, vehicle.Id, Paying(vehicle));
        }

        private void GetTarrif(Vehicle vehicle)
        {
            if (vehicle.VehicleType == VehicleType.Bus)
                _tariff = Settings.Bus;
            else if (vehicle.VehicleType == VehicleType.Motorcycle)
                _tariff = Settings.Motorcycle;
            else if (vehicle.VehicleType == VehicleType.PassengerCar)
                _tariff = Settings.PassengerCar;
            else if (vehicle.VehicleType == VehicleType.Truck) _tariff = Settings.Truck;
        }

        private decimal Paying(Vehicle vehicle)
        {
            if (vehicle.Balance > _tariff)
            {
                vehicle.Balance -= _tariff;
                Parking.Source.Balance += _tariff;
                return _tariff;
            }

            if ((vehicle.Balance < _tariff) & (vehicle.Balance >= 0))
            {
                var set = vehicle.Balance + (_tariff - vehicle.Balance) * Settings.PenaltyRate;
                vehicle.Balance -= set;
                Parking.Source.Balance += set;
                return vehicle.Balance + (_tariff - vehicle.Balance) * Settings.PenaltyRate;
            }

            vehicle.Balance -= _tariff * Settings.PenaltyRate;
            Parking.Source.Balance += _tariff * Settings.PenaltyRate;
            return _tariff * Settings.PenaltyRate;
        }
    }
}