﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer _timer;

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer = new Timer
            {
                Interval = Interval
            };
            _timer.Elapsed += FireElapsedEvent;
            _timer.AutoReset = true;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void FireElapsedEvent(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(sender, e);
        }
    }
}