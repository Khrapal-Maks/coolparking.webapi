﻿using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime DateTime { get; set; }

        public string Id { get; set; }

        public decimal Sum { get; set; }

        public TransactionInfo(DateTime dateTime, string id, decimal sum)
        {
            DateTime = dateTime;
            Id = id;
            Sum = sum;
        }
    }
}