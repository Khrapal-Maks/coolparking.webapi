﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        private readonly string _logFilePath = @"Transactions.log.";
        private static readonly Lazy<Parking> lazy =
        new(() => new Parking());
        private ParkingService _parkingService;
        private ILogService _logService;
        private ITimerService _logTimer;
        private ITimerService _withdrawTimer;

        public ParkingService ParkingService { get; }

        public List<Vehicle> AllTransports { get; }

        public decimal Balance { get; set; }

        public static Parking Source { get { return lazy.Value; } }

        static Parking() { }

        private Parking()
        {
            GetServices();
            ParkingService = _parkingService;
            AllTransports = new List<Vehicle>
            {
                Capacity = Settings.Capacity
            };
            Balance = Settings.InitialBalance;
        }

        private void GetServices()
        {
            _logTimer = new TimerService
            {
                Interval = Settings.LoggingPeriod
            };
            _withdrawTimer = new TimerService
            {
                Interval = Settings.PaymentChargePeriod
            };
            _logService = new LogService(_logFilePath);
            _parkingService = new ParkingService(_withdrawTimer, _logTimer, _logService);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            AllTransports.Clear();
            Balance = 0;
        }

        ~Parking()
        {
            Dispose(false);
        }
    }
}