﻿using System;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private const string Simple = "XX-YYYY-XX";
        private const string Chars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        private const string Numbers = "0123456789";

        public string Id { get; }

        public VehicleType VehicleType { get; }

        public decimal Balance { get; set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            ChechingFormatId(id);
            ChecingBalance(balance);
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }

        private static void ChecingBalance(decimal balance)
        {
            if (balance < 0) throw new ArgumentException();
        }

        private static void ChechingFormatId(string id)
        {
            string pattern = "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";

            if (!Regex.IsMatch(id, pattern))
            {
                throw new ArgumentException();
            }
        }

        private static string GenerateRandomRegistrationPlateNumber()
        {
            var identifier = new StringBuilder();

            Random r = new();

            foreach (var item in Simple)
                switch (item)
                {
                    case 'X':
                        identifier.Append(Chars[r.Next(0, Chars.Length)]);
                        break;
                    case 'Y':
                        identifier.Append(Numbers[r.Next(0, Numbers.Length)]);
                        break;
                    default:
                        identifier.Append(item);
                        break;
                }

            return identifier.ToString();
        }

        public static Vehicle CreateRandomVenicle()
        {
            Random random = new();
            VehicleType[] vehicleTypes = Enum.GetValues(typeof(VehicleType)).Cast<VehicleType>().ToArray();
            return new Vehicle(GenerateRandomRegistrationPlateNumber(), vehicleTypes[random.Next(0, vehicleTypes.Length)], random.Next(0, 100));
        }
    }
}