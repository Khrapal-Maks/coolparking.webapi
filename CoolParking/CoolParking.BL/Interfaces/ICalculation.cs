﻿using CoolParking.BL.Models;
using System;

namespace CoolParking.BL.Interfaces
{
    public interface ICalculation
    {
        TransactionInfo WeCalculatePayment(Vehicle vehicle, DateTime dateTime);
    }
}
