﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ConsoleCheif
{
    public class Parking 
    {
        [JsonPropertyName("allTransports")]
        public List<Vehicle> AllTransports { get; set; }

        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }
    }
}