﻿using CoolParking.WebAPI.Interface;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Models
{
    public class Validation : IValidation
    {
        private const string Simple = "XX-YYYY-XX";
        private const string Chars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        private const string Numbers = "0123456789";

        public bool ValidationFormatId(string id)
        {
            string pattern = "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";

            if (!Regex.IsMatch(id, pattern))
            {
                return false;
            }
            return true;
        }
    }
}
