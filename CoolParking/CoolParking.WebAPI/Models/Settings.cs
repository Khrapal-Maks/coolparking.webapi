﻿namespace CoolParking.WebAPI.Models
{
    public static class Settings
    {
        public const decimal InitialBalance = 0;

        public const int Capacity = 10;

        public const double PaymentChargePeriod = 5000;

        public const double LoggingPeriod = 60000;

        public const decimal PassengerCar = 2;

        public const decimal Truck = 5;

        public const decimal Bus = 3.5m;

        public const decimal Motorcycle = 1;

        public const decimal PenaltyRate = 2.5m;
    }
}