﻿using CoolParking.WebAPI.Interface;
using CoolParking.WebAPI.Services;
using System;
using System.Collections.Generic;

namespace CoolParking.WebAPI.Models
{
    public class Parking : IDisposable
    {
        private readonly string _logFilePath = @"Transactions.log.";
        private ParkingService _parkingService;
        private ILogService _logService;
        private ITimerService _logTimer;
        private ITimerService _withdrawTimer;

        public ParkingService ParkingService { get; }

        public List<Vehicle> AllTransports { get; }

        public decimal Balance { get; set; }

        public Parking()
        {
            GetServices();
            ParkingService = _parkingService;
            AllTransports = new List<Vehicle>
            {
                Capacity = Settings.Capacity
            };
            Balance = Settings.InitialBalance;
        }

        private void GetServices()
        {
            _logTimer = new TimerService
            {
                Interval = Settings.LoggingPeriod
            };
            _withdrawTimer = new TimerService
            {
                Interval = Settings.PaymentChargePeriod
            };
            _logService = new LogService(_logFilePath);
            _parkingService = new ParkingService(this, _withdrawTimer, _logTimer, _logService);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            AllTransports.Clear();
            Balance = 0;
        }

        ~Parking()
        {
            Dispose(false);
        }
    }
}