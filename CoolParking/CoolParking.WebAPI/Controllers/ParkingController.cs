﻿using CoolParking.WebAPI.Interface;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api")]
    //[Produces("application/json")]
    public class ParkingController : Controller
    {
        private readonly ILogger<Parking> _logger;
        private readonly IValidation _validation;
        private readonly Parking _parking;

        public ParkingController(ILogger<Parking> logger, IValidation validation, Parking parking)
        {
            _validation = validation;
            _parking = parking;
            _logger = logger;
        }

        //GET api/parking/balance
        //Response:
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: decimal
        //Body example: 10.5

        /// <summary>
        /// Get Balance Parking. 
        /// </summary>
        /// <returns>decimal</returns>
        [HttpGet]
        [Route("parking/balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(_parking.ParkingService.GetBalance());
        }

        //GET api/parking/capacity
        //Response:
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: int
        //Body example: 10

        /// <summary>
        /// Get Capacity Parking. 
        /// </summary>
        /// <returns>int</returns>
        [HttpGet]
        [Route("parking/capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(_parking.ParkingService.GetCapacity());
        }

        //GET api/parking/freePlaces
        //Response:
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: int
        //Body example: 9

        /// <summary>
        /// Get free places Parking. 
        /// </summary>
        /// <returns>int</returns>
        [HttpGet]
        [Route("parking/freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(_parking.ParkingService.GetFreePlaces());
        }

        //GET api/vehicles
        //Response:
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: [{ “id”: string, “vehicleType”: int, "balance": decimal}]
        //Body example: [{ “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }]

        /// <summary>
        /// Get all transports on Parkihg
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("vehicles")]
        public ActionResult<Vehicle[]> GetVehicles()
        {
            return Ok(_parking.ParkingService.GetVehicles());
        }

        //GET api/vehicles/id(id - vehicle id of format “AA-0001-AA”)
        //Response:
        //If id is invalid - Status Code: 400 Bad Request
        //If vehicle not found - Status Code: 404 Not Found
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: { “id”: string, “vehicleType”: int, "balance": decimal}
        //Body example: { “id”: “GP-5263-GC”, “vehicleType”: 2, "balance": 196.5 }

        /// <summary>
        /// Find this venicle
        /// </summary>
        /// <returns>Venicle or Status Code</returns>
        [HttpGet]
        [Route("vehicles/{id}")]
        public ActionResult<Vehicle> FindVehicle(string id)
        {      
            if(!_validation.ValidationFormatId(id))
            {
                return BadRequest();
            }

            if(_parking.ParkingService.GetVehicles().ToList().Find(x => x.Id == id) == null)
            {
                return NotFound();
            }

            return Ok(_parking.ParkingService.GetVehicles().ToList().Find(x => x.Id == id));
        }

        //POST api/vehicles
        //Request:
        //Body schema: { “id”: string, “vehicleType”: int, “balance”: decimal}
        //Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, “balance”: 100 }
        //Response:
        //If body is invalid - Status Code: 400 Bad Request
        //If request is handled successfully
        //Status Code: 201 Created
        //Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
        //Body example: { “id”: “LJ - 4812 - GL”, “vehicleType”: 2, "balance": 100 }

        /// <summary>
        /// Add venicle
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// Body schema: { “id”: string, “vehicleType”: int, “balance”: decimal}
        /// Body example: { “id”: “LJ-4812-GL”, “vehicleType”: 2, “balance”: 100 }
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="vehicleType"></param>
        /// <param name="balance"></param>
        /// <returns>Venicle or Status Code</returns>
        [HttpPost]
        [Route("vehicles/{id}/{vehicleType:int}/{balance:decimal}")]
        public ActionResult<Vehicle> AddVehicle(string id, VehicleType vehicleType, decimal balance)
        {  
            var venicle = new Vehicle(id, vehicleType, balance);

            if (!TryValidateModel(venicle, nameof(Vehicle)))
            {
                return BadRequest();
            }

            _parking.ParkingService.AddVehicle(venicle);
            return Created("201", _parking.ParkingService.GetVehicles().ToList().Find(x => x.Id == id)); 
        }

        //DELETE api/vehicles/id(id - vehicle id of format “AA-0001-AA”)
        //Response:
        //If id is invalid - Status Code: 400 Bad Request
        //If vehicle not found - Status Code: 404 Not Found
        //If request is handled successfully
        //Status Code: 204 No Content

        /// <summary>
        /// Remove venicle
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Venicle or Status Code</returns>
        [HttpDelete]
        [Route("vehicles/{id}")]
        public ActionResult RemoveVehicle(string id)
        {
            if (!_validation.ValidationFormatId(id))
            {
                return BadRequest("id is invalid");
            }

            if (_parking.ParkingService.GetVehicles().ToList().Find(x => x.Id == id) == null)
            {
                return NotFound();
            }

            _parking.ParkingService.RemoveVehicle(id);
            return NoContent();
        }

        //GET api/transactions/last
        //Response:
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: [{ “vehicleId”: string, “sum”: decimal, "transactionDate": DateTime}]
        //Body example: [{ “vehicleId”: "DG-3024-UB", “sum”: 3.5, "transactionDate": "2020-05-10T11:36:20.6395402+03:00"}]

        /// <summary>
        /// Get last Transactions from collection. Before write in log faile.
        /// </summary>
        /// <returns>Collection Transactions</returns>
        [HttpGet]
        [Route("transactions/last")]
        public ActionResult<TransactionInfo[]> GetLastParkingTransactions()
        {
            var buffer = new List<TransactionInfo>(_parking.ParkingService.GetLastParkingTransactions());
            return Ok(buffer.ToArray());
        }

        //GET api/transactions/all(только транзакции с лог файла)
        //Response:
        //If log file not found - Status Code: 404 Not Found
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: string
        //Body example: “5/10/2020 11:21:20 AM: 3.50 money withdrawn from vehicle with Id = 'GP-5263-GC'.\n5/10/2020 11:21:25 AM: 3.50 money withdrawn from vehicle with Id = 'GP-5263-GC'.”

        /// <summary>
        /// Get transactions from log file
        /// </summary>
        /// <returns>Collection Transactions from log</returns>
        [HttpGet]
        [Route("transactions/all")]
        public ActionResult<TransactionInfo[]> ReadFromLog()
        {
            if (_parking.ParkingService.ReadFromLog() == null)
            {
                return NotFound();
            }
            return Ok(_parking.ParkingService.ReadFromLog());
        }

        //PUT api/transactions/topUpVehicle
        //Body schema: { “id”: string, “Sum”: decimal}
        //Body example: { “id”: “GP-5263-GC”, “Sum”: 100 }
        //Response:
        //If body is invalid - Status Code: 400 Bad Request
        //If vehicle not found - Status Code: 404 Not Found
        //If request is handled successfully
        //Status Code: 200 OK
        //Body schema: { “id”: string, “vehicleType”: int, "balance": decimal }
        //Body example: { “id”: “GP - 5263 - GC”, “vehicleType”: 2, "balance": 245.5 }

        [HttpPut]
        [Route("transactions/topUpVehicle/{id?}/{Sum:decimal?}")]
        public ActionResult TopUpVehicle(string id, decimal Sum)
        {
            if(id == null & Sum != 0 & id != null & Sum == 0)
            {
                return BadRequest();
            }

            if (_parking.ParkingService.GetVehicles().ToList().Find(x => x.Id == id) == null)
            {
                return NotFound();
            }

            _parking.ParkingService.TopUpVehicle(id, Sum);
            return Ok(_parking.ParkingService.GetVehicles().ToList().Find(x => x.Id == id));   
        }
    }
}
