﻿using CoolParking.WebAPI.Interface;
using CoolParking.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Timers;

namespace CoolParking.WebAPI.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ICalculation _calculationService;
        private readonly ILogService _logService;
        private readonly ITimerService _logTimer;
        private readonly ITimerService _withdrawTimer;
        private Parking _parking;

        public List<TransactionInfo> Transactions { get; }

        public ParkingService(Parking parking, ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = parking;
            Transactions = new List<TransactionInfo>();
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _calculationService = new CalculationService(_parking);
            GetTimers();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle == null)
            {
                throw new ArgumentException("Invalid vehicle");
            }

            if (_parking.AllTransports.Select(x => x.Id).Contains(vehicle.Id))
            {
                throw new ArgumentException();
            }

            if (ValidationParkingCapacity())
            {
                _parking.AllTransports.Add(vehicle);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Transport is parked: ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"\nID:      {vehicle.Id}\nType:    {vehicle.VehicleType}\nBalance: {vehicle.Balance}");
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Dispose()
        {
            _parking.Dispose();
        }

        public decimal GetBalance()
        {
            return _parking.Balance;
        }

        public int GetCapacity()
        {
            return _parking.AllTransports.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.AllTransports.Capacity - _parking.AllTransports.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new(_parking.AllTransports);
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!_parking.AllTransports.Select(x => x.Id).Contains(vehicleId))
            {
                throw new ArgumentException("Not found ID");
            }

            if (_parking.AllTransports.Find(x => x.Id == vehicleId).Balance >= 0)
            {
                _parking.AllTransports.Remove(_parking.AllTransports.Find(x => x.Id == vehicleId));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Transport was issued");
            }
            else
            {
                throw new InvalidOperationException();
            }
        }


        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0)
            {
                throw new ArgumentException();
            }

            if (!_parking.AllTransports.Select(x => x.Id).Contains(vehicleId))
            {
                throw new ArgumentException();
            }
            _parking.AllTransports.Find(x => x.Id == vehicleId).Balance += sum;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("The balance has been replenished");
        }

        private void GetTimers()
        {
            _withdrawTimer.Elapsed += WithdrawTimer_WriteOff;
            _withdrawTimer.Start();
            _logTimer.Elapsed += LogTimer_WriteOff;
            _logTimer.Start();
        }

        private void WithdrawTimer_WriteOff(object sender, ElapsedEventArgs e)
        {
            foreach (var itemAllTransport in _parking.AllTransports)
            {
                Transactions.Add(_calculationService.WeCalculatePayment(itemAllTransport, DateTime.Now));
            }
        }

        private void LogTimer_WriteOff(object sender, ElapsedEventArgs e)
        {
            StringBuilder text = new($"New logging period {DateTime.Now} Balance Parking: {_parking.Balance} Sum all transactions: {_parking.ParkingService.Transactions.Sum(x => x.Sum)}");
            foreach (TransactionInfo itemTransaction in Transactions.ToList())
            {
                text.Append($"*{itemTransaction.DateTime}  with ID:  {itemTransaction.Id}  the money was written off in the amount:  {itemTransaction.Sum}.");
            }
            _logService.Write(text.ToString());
            Transactions.Clear();
        }

        private bool ValidationParkingCapacity()
        {
            return _parking.AllTransports.Count < Settings.Capacity;
        }
    }
}