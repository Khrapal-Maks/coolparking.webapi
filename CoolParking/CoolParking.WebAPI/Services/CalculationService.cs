﻿using CoolParking.WebAPI.Interface;
using CoolParking.WebAPI.Models;
using System;

namespace CoolParking.WebAPI.Services
{
    public class CalculationService : ICalculation
    { 
        private decimal _tariff;
        private Parking _parking;

        public CalculationService(Parking parking)
        {
            _parking = parking;
        }       

        public TransactionInfo WeCalculatePayment(Vehicle vehicle, DateTime dateTime)
        {
            GetTarrif(vehicle);
            return new TransactionInfo(dateTime, vehicle.Id, Paying(vehicle));
        }

        private void GetTarrif(Vehicle vehicle)
        {
            if (vehicle.VehicleType == VehicleType.Bus)
                _tariff = Settings.Bus;
            else if (vehicle.VehicleType == VehicleType.Motorcycle)
                _tariff = Settings.Motorcycle;
            else if (vehicle.VehicleType == VehicleType.PassengerCar)
                _tariff = Settings.PassengerCar;
            else if (vehicle.VehicleType == VehicleType.Truck) _tariff = Settings.Truck;
        }

        private decimal Paying(Vehicle vehicle)
        {
            if (vehicle.Balance > _tariff)
            {
                vehicle.Balance -= _tariff;
                _parking.Balance += _tariff;
                return _tariff;
            }

            if ((vehicle.Balance < _tariff) & (vehicle.Balance >= 0))
            {
                var set = vehicle.Balance + (_tariff - vehicle.Balance) * Settings.PenaltyRate;
                vehicle.Balance -= set;
                _parking.Balance += set;
                return vehicle.Balance + (_tariff - vehicle.Balance) * Settings.PenaltyRate;
            }

            vehicle.Balance -= _tariff * Settings.PenaltyRate;
            _parking.Balance += _tariff * Settings.PenaltyRate;
            return _tariff * Settings.PenaltyRate;
        }
    }
}