﻿using CoolParking.WebAPI.Interface;
using System;
using System.IO;
using System.Text;

namespace CoolParking.WebAPI.Services
{
    public class LogService : ILogService
    {
        private string _line;

        private string _logFilePath = $@"{Path.GetFullPath("Transactions.log")}";

        public string LogPath { get; }

        public LogService(string logFilePath)
        {
            LogPath = logFilePath;
        }

        public string Read()
        {
            try
            {
                try
                {
                    if (File.Exists(_logFilePath) == false)
                    {
                        throw new InvalidOperationException();
                    }
                    else
                    {
                        using StreamReader sr = new(_logFilePath);
                        _line = sr.ReadToEnd();
                        sr.Close();
                    }
                }
                catch
                {
                    throw;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _line;
        }

        public void Write(string logInfo)
        {
            try
            {
                using StreamWriter sw = new(LogPath, true, Encoding.Default);
                var text = logInfo.Split(new[] { '*' });
                foreach (string item in text)
                {
                    sw.WriteLine(item);                    
                }
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}