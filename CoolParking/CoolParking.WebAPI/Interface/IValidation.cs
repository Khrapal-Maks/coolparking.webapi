﻿namespace CoolParking.WebAPI.Interface
{
    public interface IValidation
    {
        bool ValidationFormatId(string id);
    }
}
