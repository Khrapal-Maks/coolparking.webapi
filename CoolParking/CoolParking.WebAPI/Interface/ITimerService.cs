﻿using System.Timers;

namespace CoolParking.WebAPI.Interface
{
    public interface ITimerService
    {
        double Interval { get; set; }
        event ElapsedEventHandler Elapsed;
        void Start();
        void Stop();
        void Dispose();        
    }
}