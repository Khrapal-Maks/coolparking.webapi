﻿using CoolParking.WebAPI.Models;
using System;

namespace CoolParking.WebAPI.Interface
{
    public interface ICalculation
    {
        TransactionInfo WeCalculatePayment(Vehicle vehicle, DateTime dateTime);
    }
}
