﻿namespace CoolParking.WebAPI.Interface
{
    public interface ILogService
    {
        string LogPath { get; }
        void Write(string logInfo);
        string Read();
    }
}