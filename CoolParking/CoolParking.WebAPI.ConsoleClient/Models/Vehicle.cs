﻿using System;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.ConsoleClientFirst
{
    public class Vehicle
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("vehicleyype")]
        public VehicleType VehicleType { get; set; }

        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }

        private const string Simple = "XX-YYYY-XX";
        private const string Chars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        private const string Numbers = "0123456789";

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {            
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }        

        private static string GenerateRandomRegistrationPlateNumber()
        {
            var identifier = new StringBuilder();

            Random r = new();

            foreach (var item in Simple)
                switch (item)
                {
                    case 'X':
                        identifier.Append(Chars[r.Next(0, Chars.Length)]);
                        break;
                    case 'Y':
                        identifier.Append(Numbers[r.Next(0, Numbers.Length)]);
                        break;
                    default:
                        identifier.Append(item);
                        break;
                }

            return identifier.ToString();
        }

        public static Vehicle CreateRandomVenicle()
        {
            Random random = new();
            VehicleType[] vehicleTypes = Enum.GetValues(typeof(VehicleType)).Cast<VehicleType>().ToArray();
            return new Vehicle(GenerateRandomRegistrationPlateNumber(), vehicleTypes[random.Next(0, vehicleTypes.Length)], random.Next(0, 100));
        }
    }
}