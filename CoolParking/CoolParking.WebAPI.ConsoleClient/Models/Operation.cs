﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.ConsoleClientFirst.Models
{
    public class Operation
    {
        private const string API_PATH = "https://localhost:44338";

        private HttpClient _client = new HttpClient();
        private decimal _sum;

        public Operation()
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        public async Task GetBalance()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            var response = await _client.GetAsync($"{API_PATH}/api/parking/balance");
            response.EnsureSuccessStatusCode();
            Console.WriteLine($"Parking balance: {await response.Content.ReadAsStringAsync()}");
        }

        public async Task GetCurrentBalance()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            var response = await _client.GetAsync($"{API_PATH}/api/transactions/last");
            response.EnsureSuccessStatusCode();
            Console.WriteLine($"Parking balance for the current period: {JsonConvert.DeserializeObject<List<TransactionInfo>>(await response.Content.ReadAsStringAsync()).Sum(x => x.Sum)}");
        }

        public async Task GetFreePlaces()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            var response = await _client.GetAsync($"{API_PATH}/api/parking/freePlaces");
            response.EnsureSuccessStatusCode();
            Console.WriteLine($"Free parking spaces: {await response.Content.ReadAsStringAsync()}");
        }

        public async Task GetLastTranceactions()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            var response = await _client.GetAsync($"{API_PATH}/api/transactions/last");
            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<List<TransactionInfo>>(await response.Content.ReadAsStringAsync());
            Console.WriteLine("Parking transactions for the current period");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0,-20} | {1,-15} | {2,-11}", "DateTime", "Id", "Sum");
            foreach (TransactionInfo item in result)
            {
                Console.WriteLine("{0,-20} | {1,-15} | {2,-11}", item.DateTime, item.Id, item.Sum);
            }
        }

        public async Task GetLogTranceactions()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            var response = await _client.GetAsync($"{API_PATH}/api/transactions/all");
            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<string>(await response.Content.ReadAsStringAsync());
            Console.WriteLine("Transaction history");
            Console.ForegroundColor = ConsoleColor.White;
            if (result == null)
            {
                Console.WriteLine("Log is empty");
            }
            else
            {
                foreach (var item in result.Split('r'))
                {
                    Console.Write(item);
                }
            }
        }

        public async Task TopUpBalanceVenicle()
        {
            Console.Clear();
            Console.WriteLine("Enter ID");
            string id = Console.ReadLine();
            var response = await _client.GetAsync($"{API_PATH}/api/vehicles/{id}");
            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<Vehicle>(await response.Content.ReadAsStringAsync());
            response = await _client.PutAsJsonAsync($"{API_PATH}/transactions/topUpVehicle/{id}/{ValidationSum()}", result);
            Console.WriteLine(response.Headers.Location);
        }

        public async Task GetAllvenicles()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            var response = await _client.GetAsync($"{API_PATH}/api/vehicles");
            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<List<Vehicle>>(await response.Content.ReadAsStringAsync());
            Console.WriteLine("All transport  in parking\n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0,-10} | {1,-15} | {2,-11}", "ID", "Type", "Balance");
            foreach (var item in result)
            {
                Console.WriteLine("{0,-10} | {1,-15} | {2,-11}", item.Id, item.VehicleType, item.Balance);
            }
        }

        public async Task<Uri> AddVenicle(Vehicle venicle)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            int venicleNumber = 0;
            var arr = Enum.GetNames(typeof(VehicleType));
            for (int i = 0; i < arr.Length; i++)
            {
                if (venicle.VehicleType.ToString() == arr[i])
                    venicleNumber = i;
            }
            var response = await _client.PostAsJsonAsync($"{API_PATH}/api/vehicles/{venicle.Id}/{venicleNumber}/{venicle.Balance}", venicle);
            response.EnsureSuccessStatusCode();
            response = await _client.GetAsync($"{API_PATH}/api/vehicles/{venicle.Id}");
            var result = JsonConvert.DeserializeObject<Vehicle>(await response.Content.ReadAsStringAsync());
            Console.WriteLine("Transport is parked: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine($"\nID:      {result.Id}\nType:    {result.VehicleType}\nBalance: {result.Balance}");
            return response.Headers.Location;
        }

        public async Task RemoveVenicle()
        {
            Console.Clear();
            Console.WriteLine("Enter ID");
            string id = Console.ReadLine();
            var response = await _client.GetAsync($"{API_PATH}/api/vehicles/{id}");
            response.EnsureSuccessStatusCode();
            response = await _client.DeleteAsync($"{API_PATH}/api/vehicles/{id}");
            response.EnsureSuccessStatusCode();
            Console.WriteLine(response.RequestMessage);
        }

        private decimal ValidationSum()
        {
            Console.WriteLine("Enter sum");
            while (!decimal.TryParse(Console.ReadLine(), out _sum))
            {
                Error();
            }
            return _sum;
        }

        private void Error()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Ошибка ввода, проверьте правильность ввода!");
            ConsoleClearForMenu();
        }

        private static void ConsoleClearForMenu()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n\n\nPress 'enter' to return to main menu...");
            Console.ReadKey();
            Console.Clear();
            return; 
        }
    }
}
