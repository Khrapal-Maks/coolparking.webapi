﻿using System;
using System.Text.Json.Serialization;

namespace CoolParking.WebAPI.ConsoleClientFirst
{
    public struct TransactionInfo
    {
        [JsonPropertyName("datetime")]
        public DateTime DateTime { get; set; }

        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("sum")]
        public decimal Sum { get; set; }
    }
}