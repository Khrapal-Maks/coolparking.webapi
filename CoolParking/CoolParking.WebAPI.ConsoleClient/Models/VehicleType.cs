﻿namespace CoolParking.WebAPI.ConsoleClientFirst
{
    public enum VehicleType
    {
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}