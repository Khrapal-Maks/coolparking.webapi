﻿using CoolParking.WebAPI.ConsoleClientFirst.Models;
using System;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.ConsoleClientFirst
{
    public class Menu
    {    
        private Operation _operation;

        public Menu()
        {
            GetMenuInterface();
        }

        private void GetMenuInterface()
        {
            _operation = new();
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Console worker");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Name operation:                                                     Select operation\n" +
                              "                                                                    and enter number. ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(
                "Вывести на экран текущий баланс Парковки:                                  1\n" +
                "Вывести на экран сумму заработанных денег за текущий период:               2\n" +
                "Вывести на экран количество свободных/занятых мест на парковке:            3\n" +
                "Вывести на экран все Транзакции Парковки за текущий период:                4\n" +
                "Вывести историю транзакций:                                                5\n" +
                "Вывести на экран список Тр. средств находящихся на Паркинге:               6\n" +
                "Поставить Тр. средство на Паркинг:                                         7\n" +
                "Забрать транспортное средство с Паркинга:                                  8\n" +
                "Пополнить баланс конкретного Тр. средства:                                 9\n");

            ValidationMenu();
        }

        private void GetOperation(int selection)
        {
            switch (selection)
            {
                case 1:
                    Task.Run(() => _operation.GetBalance()).Wait();
                    ConsoleClearForMenu();
                    break;
                case 2:                    
                    Task.Run(() => _operation.GetCurrentBalance()).Wait();                    
                    ConsoleClearForMenu();
                    break;
                case 3:
                    Task.Run(() => _operation.GetFreePlaces()).Wait();
                    ConsoleClearForMenu();
                    break;
                case 4:
                    Task.Run(() => _operation.GetLastTranceactions()).Wait();
                    ConsoleClearForMenu();                    
                    break;
                case 5:
                    Task.Run(() => _operation.GetLogTranceactions()).Wait();
                    ConsoleClearForMenu();
                    break;
                case 6:
                    Task.Run(() => _operation.GetAllvenicles()).Wait();
                    ConsoleClearForMenu();                    
                    break;
                case 7:
                    var venicle = Vehicle.CreateRandomVenicle();
                    Task.Run(() => _operation.AddVenicle(venicle)).Wait();
                    ConsoleClearForMenu();
                    break;
                case 8:
                    Task.Run(() => _operation.RemoveVenicle()).Wait();
                    ConsoleClearForMenu();
                    break;
                case 9:
                    Task.Run(() => _operation.TopUpBalanceVenicle()).Wait();
                    ConsoleClearForMenu();
                    break;
                default:
                    Console.Clear();
                    Error();
                    break;
            }
        }   

        private void ValidationMenu()
        {
            int selection;

            while (!int.TryParse(Console.ReadLine(), out selection))
            {
                Error();
            }
            GetOperation(selection);
        }        

        private void ConsoleClearForMenu()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\n\n\nPress 'enter' to return to main menu...");
            Console.ReadKey();
            Console.Clear();
            GetMenuInterface();
        }

        private void Error()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Ошибка ввода, проверьте правильность ввода!");
            ConsoleClearForMenu();
        }
    }
}